Create a docker container to use as prefect database

- Pull the image
```shell
podman pull postgres:14.6
```

- Create a volume to mount the data
```shell
podman volume create oriondb
```

- Create a container
```shell
podman run -d \
    --name orion_postgres \
    -p 5431:5432 \
    -e POSTGRES_USER=root \
    -e POSTGRES_PASSWORD=root \
    -e POSTGRES_DB=orion \
    -v oriondb:/var/lib/postgresql/data \
    postgres:14.6
```

- To change the database to postgres use
```shell
prefect config set PREFECT_ORION_DATABASE_CONNECTION_URL="postgresql+asyncpg://root:root@localhost:5431/orion"
```

- To inspect your profile and be sure than the environment variable has been set propertly
```shell
prefect config view --show-sources
```
