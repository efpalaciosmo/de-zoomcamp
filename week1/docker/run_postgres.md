## Runing PostgreSQL on docker

In order to run PostgreSQL on docker we need to run the following code

```shell
docker pull postgres:15
docker run -it \
    --name=postgres_ny \
    -e POSTGRES_USER=root \
    -e POSTGRES_PASSWORD=root \
    -e POSTGRES_DB=ny_taxi \
    -v $(pwd)/ny_taxi_postgres_data:/var/lib/postgresql/data \
    -p 5431:5432 \
    postgres:15
```

To connect to the database we will use psql, run the following code

```shell
docker exec -it postgres_ny psql -U root -d ny_taxi
```
