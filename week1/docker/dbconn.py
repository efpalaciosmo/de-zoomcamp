from sqlalchemy import create_engine
from dotenv import load_dotenv
import os

load_dotenv()
user = os.environ.get('USERNAME')
password = os.environ.get('PASSWORD')
server = os.environ.get('SERVER')
port = os.environ.get('PORT')
database = os.environ.get('DATABASE')


def Connection() -> create_engine:
    """
    function that return a engine of postgresql
    user: user owner of the database
    password: password for that user
    server: server used
    port: port where the database is listening
    database: name of the database
    """
    dns = "postgresql://{}:{}@{}:{}/{}".format(
            user,
            password,
            server,
            port,
            database)
    engine = create_engine(dns)
    return engine


print(Connection())
