from bs4 import BeautifulSoup
from lxml import etree
import pandas as pd
import dbconn
import requests
from pathlib import Path


url = 'https://www.nyc.gov/site/tlc/about/tlc-trip-record-data.page'


def getData() -> pd.DataFrame:
    """Function that reuturn a dataframe with the data of all months"""
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    soup_2022 = soup.find_all(id='faq2022')
    tree = etree.HTML(str(soup_2022))
    links = tree.xpath("//li/a[@title='Yellow Taxi Trip Records']/@href")
    data = pd.read_parquet(links[0])
    pd.to_datetime(data['tpep_pickup_datetime'])
    pd.to_datetime(data['tpep_dropoff_datetime'])
    for link in links[1:]:
        df = pd.read_parquet(link)
        df['tpep_pickup_datetime'] = pd.to_datetime(df['tpep_pickup_datetime'])
        df['tpep_dropoff_datetime'] = pd.to_datetime(df['tpep_dropoff_datetime'])
        data = pd.concat([data, df])

    data.to_csv('ny_taxi.csv')


def getLookupData() -> None:
    connection = dbconn.Connection()
    data = pd.read_csv("https://d37ci6vzurychx.cloudfront.net/misc/taxi+_zone_lookup.csv")
    data.to_sql(
            name='taxi_zone_lookup',
            con=connection,
            if_exists='replace')
    print("inserted data")


def get_schema(df: pd.DataFrame) -> None:
    print(pd.io.sql.get_schema(df, name='yellow_taxi_data'))


def sendDataToPostgres(csv_path: Path) -> None:
    connection = dbconn.Connection()
    data_iter = pd.read_csv(csv_path, chunksize=100000, iterator=True)
    data = next(data_iter)
    data.head(n=0).to_sql(
            name='yellow_taxi_data',
            con=connection,
            if_exists='replace')
    while True:
        df = next(data_iter)
        df.to_sql(
                name='yellow_taxi_data',
                con=connection,
                if_exists='append')
        print("inserted chunk")


getLookupData()


# path = Path('./ny_taxi.csv')
# sendDataToPostgres(path)
