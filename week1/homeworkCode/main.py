import pandas as pd
from typing import List
from pathlib import Path
from sqlalchemy import create_engine


def downloadData(urls: List[List[str]]) -> None:
    data = pd.DataFrame()
    for url in urls:
        data = pd.read_csv(url[0])
        data.to_csv(url[1])


dns = "postgresql+psycopg2://root:root@localhost:5431/ny_taxi"
engine = create_engine(dns)


def uploadData(path: Path) -> None:
    for file in path.glob('**/*.csv'):
        data = pd.read_csv(file)
        filename = str(file).split('/')[-1]
        if filename == 'taxi_zone_lookup_s3.csv':
            data.to_sql(
                con=engine,
                name=filename.split('.')[0],
                if_exists='replace')
        print(data['lpep_pickup_datetime'])
        #data['lpep_pickup_datetime'] = pd.to_datetime(data['lpep_pickup_datetime'])
        #data['lpep_dropoff_datetime'] = pd.to_datetime(data['lpep_dropoff_datetime'])
       # data.to_sql(
       #         con=engine,
       #         name=filename.split('.')[0],
       #         if_exists='replace')


urls = [
        ["https://github.com/DataTalksClub/nyc-tlc-data/releases/download/green/green_tripdata_2019-01.csv.gz", "green_tripdata.csv"],
        ["https://s3.amazonaws.com/nyc-tlc/misc/taxi+_zone_lookup.csv", "taxi_zone_lookup_s3.csv"]
        ]

downloadData(urls)
path = Path.cwd()
uploadData(path)
